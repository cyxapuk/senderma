package com.example.and.sendmail;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private String receiverMailAddress ="mailTo@gmail.com";         //почта на которую будут приходить письма
    private String emailSubj="Mail from BAKU TAXI client";      // Тема письма
    private String senderMail = "mailFrom@meta.ua";      // почтовый ящик с которго будут уходить письма
    private String password = "passsword";      // пароль от почтового ящика с которго будут уходить письма

    private EditText etMailBody;
    private EditText etSenderMail;
    private Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etMailBody = (EditText) findViewById(R.id.editText);
        etSenderMail = (EditText) findViewById(R.id.mailFrom);
        btnSend = (Button) findViewById(R.id.btnOK);
        btnSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Mail mailSend = new Mail(senderMail, password);
        String [] to = {receiverMailAddress};       // массив получателей письма
        mailSend.setTo(to);
        mailSend.setFrom(senderMail);
        mailSend.setSubject(emailSubj);
        String mailBody = etMailBody.getText().toString()+
                "\n"+
                "Mail from ->"+getSenderMail();
        mailSend.setBody(mailBody);
        sendMail(mailSend);
    }

    private String getSenderMail(){
        String mail ;
        mail = etSenderMail.getText().toString();
        if(mail.isEmpty())
            mail = "empty mail";
        return mail;
    }

    private void sendMail(final Mail mailSend){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {

                        boolean isSuccess = mailSend.send();
                        if(isSuccess){
                            showToast( "Успешно!");
                        }else{
                            showToast("Неуспешно!");
                        }

                    } catch(Exception e) {
                        showToast("Ошибка при отправке");
                        Log.e("MailApp", "Could not send email", e);
                    }
                }

                private void showToast(final String text){
                   runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                           Toast.makeText(MainActivity.this, text, Toast.LENGTH_LONG).show();
                       }
                   });
                }
            }).start();
    }
}
